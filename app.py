#!/usr/bin/env python

# Copyright 2016 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Simple application that performs a query with BigQuery."""
# [START bigquery_simple_app_all]
# [START bigquery_simple_app_deps]
from google.cloud import bigquery
import csv
import os
# [END bigquery_simple_app_deps]


def query_stackoverflow():
    # [START bigquery_simple_app_client]
    client = bigquery.Client()
    # [END bigquery_simple_app_client]
    # [START bigquery_simple_app_query]
    query_job = client.query("""
        SELECT
          CONCAT(
            'https://stackoverflow.com/questions/',
            CAST(id as STRING)) as url,
          view_count
        FROM `bigquery-public-data.stackoverflow.posts_questions`
        WHERE tags like '%google-bigquery%'
        ORDER BY view_count DESC
        LIMIT 10""")

    results = query_job.result()  # Waits for job to complete.
    # [END bigquery_simple_app_query]

    # [START bigquery_simple_app_print]
    for row in results:
        print("{} : {} views".format(row.url, row.view_count))
    # [END bigquery_simple_app_print]


if __name__ == '__main__':
    # query_stackoverflow()
    # with open("%s/user.csv" % os.getcwd(), "r") as f:
    #     reader = csv.reader(f)
    #     i = next(reader)
    # print(i)
    # import string
    #
    # s = "408960"
    # table = str.maketrans({key: None for key in string.punctuation})
    # new_s = s.translate(table)
    # print(str(new_s).isnumeric())
    def split_list(a_list):
        half = len(a_list) // 2
        return a_list[:half], a_list[half:]
    import sys
    l1, l2 = split_list([1,2,34,5,6,67,9])
    print(sys.getsizeof(l1))
    # os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "%s/service-account-file.json" % os.getcwd()
    # print('Credendtials from environ: {}'.format(os.environ.get('GOOGLE_APPLICATION_CREDENTIALS')))
    # [END bigquery_simple_app_all]