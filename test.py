from google.cloud import bigquery
import os
import csv
import string
import time
import sys

def toCSV(input_file, output_file):

    with open(input_file) as infile, open(output_file, 'w') as outfile:
        for line in infile:
            outfile.write(line.replace('\t', ','))
    print(input_file + " was converted to " + output_file)

def get_csv_column_name(filename):
    with open("%s/%s" % (os.getcwd(), filename), "r") as f:
        reader = csv.reader(f)
        i = next(reader)
        return i

def read_csv_item(csvfile):
    result = []
    with open(csvfile, newline='') as f:
        readers = csv.reader(f)
        for row in readers:
            item = []
            for i in row:
                if i == 'None':
                    item.append(None)
                elif i == '':
                    item.append(None)
                else:
                    item.append(i)
            result.append(tuple(item))
    return result[1:]

def getSchema(csvfile):
        schema = []
        items = read_csv_item(csvfile)[1]
        for item in items:
            table = str.maketrans({key: None for key in string.punctuation})
            if item:
                new_s = item.translate(table)
                if new_s == "":
                    schema.append('STRING')
                elif 'uid' in item:
                    schema.append('STRING')
                elif '/' and ':' in str(item):
                    schema.append('TIMESTAMP')
                elif new_s.replace(' ', '').isalpha():
                    schema.append('STRING')
                elif new_s.replace(' ', '').isnumeric():
                    schema.append('NUMERIC')
                elif new_s.replace(' ', '').isalnum():
                    schema.append('STRING')
            else:
                schema.append('STRING')
        return schema

def bq_create_dataset(dataset_id):
    bigquery_client = bigquery.Client()
    dataset_ref = bigquery_client.dataset(dataset_id=dataset_id)

    try:
        bigquery_client.get_dataset(dataset_ref)
    except:
        dataset = bigquery.Dataset(dataset_ref)
        dataset = bigquery_client.create_dataset(dataset)
        print('Dataset {} created. '.format(dataset.dataset_id))

def bq_create_table(dataset_id, table_id, filename):
    bigclient_client = bigquery.Client()
    dataset_ref = bigclient_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_id)

    try:
        bigclient_client.get_table(table_ref)
    except:
        col_and_types = []
        schema = []
        cols = get_csv_column_name(filename)
        types = getSchema(filename)
        for col, type in zip(cols, types):
            col_and_types.append(dict(col=col, type=type))

        for item in col_and_types:
            if 'uid' in item.get('col'):
                schema.append(bigquery.SchemaField(item.get('col').replace(" ", "_"), "STRING", description=item.get('col')))
            else:
                schema.append(bigquery.SchemaField(item.get('col').replace(" ", "_"), item.get('type'),
                                                   description=item.get('col')))

        table = bigquery.Table(table_ref, schema=schema)
        table = bigclient_client.create_table(table)
        print('table {} created'.format(table.table_id))


def export_items_to_bigquery(dataset_id, table_id, csvfile):
    bigquery_client = bigquery.Client()

    dataset_ref = bigquery_client.dataset(dataset_id)

    table_ref = dataset_ref.table(table_id)
    table = bigquery_client.get_table(table_ref) # API Call

    rows_to_insert = read_csv_item(csvfile=csvfile)
    try:
        # inserted_row = 0
        # total_row = len(rows_to_insert)
        # for row in rows_to_insert:
        #     bigquery_client.insert_rows(table, [row])
        #     print("%s/%s" % (inserted_row, total_row))
        #     inserted_row += 1
        bigquery_client.insert_rows(table, rows_to_insert)
        print("inserting task done..")
    except Exception as e:
        print("ERROR")
        print(e)




def get_single_row_by_uid(uid, dataset_id, table_id):
    bigquery_client = bigquery.Client()

    query = ('SELECT * FROM `{}.{}.{}` WHERE uid="{}" LIMIT 1'
             .format('daruma-data-dev', dataset_id, table_id, uid))

    try:
        query_job = bigquery_client.query(query)
        is_exist = len(list(query_job.result())) >= 1
        # print("Exist uid: {}".format(uid)) if is_exist else "Not exist uid: {}".format(uid)
        if is_exist:
            for i in query_job.result():
                return dict(uid=i.uid,
                            name=i.name,
                            legal_entity_type=i.legal_entity_type,
                            directory__name=i.directory__name)
        else:
            return None
    except Exception as e:
        print("Error: ")
        print(e)

def isEmpty(dataset_id, table_id):
    bigquery_client = bigquery.Client()

    query = ('SELECT * FROM `{}.{}.{}` LIMIT 1'
             .format('daruma-data-dev', dataset_id, table_id))

    try:
        query_job = bigquery_client.query(query)
        if len(list(query_job.result())) > 0:
            return False
        else:
            return True
    except Exception as e:
        print("Error: ")
        print(e)

def pushToBigquery(filename):
    toCSV(input_file="%s/Backoffice Data/%s" % (os.getcwd(), filename),
          output_file=filename.replace(".txt", ".csv"))
    dataset_id = filename.replace(" ", "_").replace(".txt", "_dataset")
    table_id = filename.replace(" ", "_").replace(".txt", "_table")
    bq_create_dataset(dataset_id=dataset_id)
    bq_create_table(dataset_id=dataset_id,
                    table_id=table_id,
                    filename=filename.replace(".txt", ".csv"))
    export_items_to_bigquery(dataset_id=dataset_id,
                             table_id=table_id,
                             csvfile=filename.replace(".txt", ".csv"))
    while True:
        if isEmpty(dataset_id=dataset_id, table_id=table_id):
            print("table is empty, trying to reinsert data into table..")
            export_items_to_bigquery(dataset_id=dataset_id,
                                     table_id=table_id,
                                     csvfile=filename.replace(".txt", ".csv"))
        else:
            print("data inserted")
            break

if __name__ == '__main__':
    # print(get_csv_column_name(filename="daruma_sales_order.csv"))
    # print(read_csv_item("daruma_sales_order.csv")[2])
    # Store_Products error Request payload size exceeds the limit: 10485760 bytes
    # print(read_csv_item("Store_Products.csv"))
    # print(get_single_row_by_uid(dataset_id="daruma_purchase_order", table_id="daruma_purchase_order", uid=949700356))
    # print(get_single_row_by_uid(uid="949700356", dataset_id="daruma_organization_dataset", table_id="daruma_organization_table"))
    filename = "daruma_purchase_order_lines.txt"
    pushToBigquery(filename)
    # col_and_types = []
    # cols = get_csv_column_name(filename)
    # types = getSchema(filename)
    # for col, type in zip(cols, types):
    #     col_and_types.append(dict(col=col, type=type))
    # print(col_and_types)
    # rows = read_csv_item('daruma_Store_Products.csv')
    # print(sys.getsizeof(rows))
    # print([rows[0]])
    # print(read_csv_item("daruma_purchase_order_lines.csv"))
    # print(isEmpty(dataset_id="daruma_organization_dataset", table_id="daruma_organization_table"))
    # export_items_to_bigquery(dataset_id="daruma_organization_dataset", table_id="daruma_organization_table", csvfile="daruma_organization.csv")
    import sys
    # import os
    # print(os.stat('Store_Products.csv').st_size)
    # print(read_csv_item("Store_Products.csv"))
    # export_items_to_bigquery(dataset_id="Store_Google_Merchant_Feed_dataset", table_id="Store_Google_Merchant_Feed_table", csvfile="Store Google Merchant Feed.csv")
    # print(getSchema("daruma_sales_order.csv"))
    # toCSV(input_file="daruma_sales_order.txt", output_file="daruma_sales_order.csv")
    # bq_create_dataset(dataset_id="daruma_sales_order_dataset")
    # bq_create_table(dataset_id="daruma_sales_order_dataset", table_id="daruma_sales_order_table", filename="daruma_sales_order.csv")
    # export_items_to_bigquery(dataset_id="daruma_sales_order_dataset", table_id="daruma_sales_order_table", csvfile="daruma_sales_order.csv")

    # print(get_single_row_by_uid(uid="949700356", table_id="daruma_organization_table", dataset_id="daruma_organization_dataset"))